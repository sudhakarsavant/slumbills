# read data from external slumCred.json file
# only 5041 bills calculated using this script which are previously manually updated
try:
    import csv
    import pandas as pd
    import mysql.connector
    import datetime as datetime
    import json
    
except Exception as e:
    print("Some Modules are Missing ")

#read json file
myjsonfile = open('slumCred.json','r')
jsondata = myjsonfile.read()
#parse json file data
obj = json.loads(jsondata)

startTime = datetime.datetime.now()
def getConnection():
    connection = mysql.connector.connect(host = str(obj['host']), 
                                        port = str(obj['port']),
                                        database = str(obj['database']), 
                                        user = str(obj['user']), 
                                        password = str(obj['password']))
    return connection
    
print(getConnection())

last_billing_year = str(obj['last_billing_year'])
getBalance = f"""
        select consumer_id,ward,balance,billing_year,active_flag 
        from balanceEnquiryPrevious where socio_economic_group='Z' 
        and billing_year ='{last_billing_year}' and active_flag='Y' order by consumer_id;
        """

getSequence = f"""
        select consumer_id,bill_no from bill where bill_no like 'slum%' 
        and billing_year='{last_billing_year}' order by consumer_id;    
        """

def getData(balancequery):
    getBalance = balancequery 
    conn = getConnection()
    cursor = conn.cursor()
    cursor.execute(getBalance, conn)
    data = cursor.fetchall()
    conn.close()
    return data

dataset1 = pd.DataFrame(getData(getBalance))
print(f"total slum consumer count is :",len(dataset1))

dataset1.columns = ['consumer_id','ward','balance', 'billing_year', 'active_flag']
dataset1['penalty'] = round((dataset1['balance'].astype(int) * 10)/100).astype(int)
# print(dataset1.head(5))

dataset2 = pd.DataFrame(getData(getSequence))
print(f"matched bills records are :",len(dataset2))
dataset2.columns = ['consumer_id','old_bill_no']
result = dataset2['old_bill_no'].str.split("-", n = 4, expand = True)
dataset2['id']= result[3]  # generate id column for creating bill_no in sequence
dataset2['bill_no']= result[0]+"-"+result[1]+"-"+str(obj['bill_date']).replace("-", '')+"-"+dataset2['id']
# print(dataset2.head(5))

mergedData = pd.merge(dataset2, dataset1, on ='consumer_id') # merge 2 dataframe data using consumer id

mergedData.drop(['old_bill_no','id','balance','billing_year'], axis = 1, inplace= True) # drop unuseful columns
#print(mergedData.head())

mergedData['bill_date'] = str(obj['bill_date'])
mergedData['billing_year'] = str(obj['current_billing_year'])
mergedData['bill_amount'] = str(obj['slum_bill_amount']) 
mergedData['previous_reading'] = str(obj['previous_reading'])
mergedData['previous_reading_date'] = str(obj['previous_reading_date'])
mergedData['current_reading'] = str(obj['current_reading'])
mergedData['current_reading_date'] = str(obj['current_reading_date'])
mergedData['temp_remarks'] = str(obj['temp_remarks'])

#mergedData.to_csv("slumbilldata.csv")
print(f"Matched merged records are :",len(mergedData))
products_data = mergedData.values.tolist()

# inserting many records
conn = getConnection()
cursor = conn.cursor()#if required then use conn.cursor(buffered=True)
counter = 0
res = ([tuple(i) for i in products_data]) # nested list data converted into tuple
# print(res)
sql = """INSERT INTO bill (consumer_id, bill_no, ward, active_flag, penalty, bill_date, billing_year, bill_amount, previous_reading, previous_reading_date, current_reading, current_reading_date, temp_remarks) 
VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
try:
    cursor.executemany(sql,res) # Executing the SQL command
    conn.commit()# Commit your changes in the database
    print(cursor.rowcount," records inserted.")
except:
    conn.rollback()# Rolling back in case of error
    print("Error while inserting records..please check data")
conn.close() # Closing the connection

endTime = datetime.datetime.now()
print("Start time:", startTime)
print("End time:", endTime)

